import bisect
import sys

class Index(object):
    def __init__(self, t, k):
        ''' Create index from all substrings of size 'length' '''
        self.k = k  # k-mer length (k)
        self.index = []
        for i in range(len(t) - k + 1):  # for each k-mer
            self.index.append((t[i:i+k], i))  # add (k-mer, offset) pair
        self.index.sort()  # alphabetize by k-mer
    
    def query(self, p):
        ''' Return index hits for first k-mer of P '''
        kmer = p[:self.k]  # query with first k-mer
        i = bisect.bisect_left(self.index, (kmer, -1))  # binary search
        hits = []
        while i < len(self.index):  # collect matching index entries
            if self.index[i][0] != kmer:
                break
            hits.append(self.index[i][1])
            i += 1
        return hits

    def queryIndex(self, p, t):
        k = self.k
        offsets = []
        for i in self.query(p):
            if p[k:] == t[i+k:i+len(p)]:  # verify that rest of P matches
                offsets.append(i)
        return offsets


    def editDistance(self, x, y):
        # Create distance matrix
        D = []
        for i in range(len(x)+1):
            D.append([0]*(len(y)+1))
        # Initialize first row and column of matrix
        for i in range(len(x)+1):
            D[i][0] = i
        for i in range(len(y)+1):
            D[0][i] = i
        # Fill in the rest of the matrix
        for i in range(1, len(x)+1):
            for j in range(1, len(y)+1):
                distHor = D[i][j-1] + 1
                distVer = D[i-1][j] + 1
                if x[i-1] == y[j-1]:
                    distDiag = D[i-1][j-1]
                else:
                    distDiag = D[i-1][j-1] + 1
                D[i][j] = min(distHor, distVer, distDiag)
        # Edit distance is the value in the bottom right corner of the matrix
        return D[-1][-1]
    

    #t = 'ACTTGGAGATCTTTGAGGCTAGGTATTCGGGATCGAAGCTCATTTCGGGGATCGATTACGATATGGTGGGTATTCGGGA'
    #p = 'GGTATTCGGGA'

    #index = Index(t, 4)
    #print(queryIndex(p, t, index))


